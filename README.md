# Simple Online Bookstore
- Was it easy to complete the task using AI? 
R: It was a little bit more difficult than the first one.

- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics) 
R: It took me around 60 minutes.

- Was the code ready to run after generation? What did you have to change to make it usable?
R: The code wasn't ready after generation, I had to add some getters and setters that the IA didn't provide, just as a comment.

- Which challenges did you face during completion of the task?
R: I had to be really specific and the code the IA provided me was not really complete.

- Which specific prompts you learned as a good practice to complete the task?
R: Suggesting appropriate design patterns based on the requirements of your application, Providing guidance on creating modular, scalable, and maintainable code and Offering insights on popular frameworks and libraries that can simplify application development and architecture. 