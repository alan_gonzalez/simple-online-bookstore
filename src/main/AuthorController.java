import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind(@RequestParam(required = false) String name) {
        if (name != null) {
            return authorService.searchAuthorsByName(name);
        } else {
            return authorService.getAllAuthors();
        }
    }

    // Add more author-related API endpoints
}

