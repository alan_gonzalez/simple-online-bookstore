import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> searchBooksByTitle(String title) {
        return bookRepository.findByTitleContaining(title);
    }

    public List<Book> searchBooksByAuthorName(String authorName) {
        return bookRepository.findByAuthorNameContaining(authorName);
    }

    public List<Book> searchBooksByGenreName(String genreName) {
        return bookRepository.findByGenreNameContaining(genreName);
    }

    // Add more book-related methods (update, delete, etc.)
}

