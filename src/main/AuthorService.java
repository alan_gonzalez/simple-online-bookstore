import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    public Author createAuthor(Author author) {
        return authorRepository.save(author);
    }

    public List<Author> searchAuthorsByName(String name) {
        return authorRepository.findByNameContaining(name);
    }

    // Add more author-related methods (update, delete, etc.)
}
