import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/genres")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping
    public List<Genre> getAllGenres() {
        return genreService.getAllGenres();
    }

    @PostMapping
    public Genre createGenre(@RequestBody Genre genre) {
        return genreService.createGenre(genre);
    }

    @GetMapping("/search")
    public List<Genre> searchGenres(@RequestParam(required = false) String name) {
        if (name != null) {
            return genreService.searchGenresByName(name);
        } else {
            return genreService.getAllGenres();
        }
    }

    // Add more genre-related API endpoints
}

