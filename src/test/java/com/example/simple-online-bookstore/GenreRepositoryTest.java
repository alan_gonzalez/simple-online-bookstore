package test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class GenreRepositoryTest {

    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testCreateAndRetrieveGenre() {
        // Create a Genre entity and save it to the repository
        Genre genre = new Genre();
        genre.setName("Sample Genre");
        genreRepository.save(genre);

        // Retrieve the genre from the repository and assert its attributes
        Genre retrievedGenre = genreRepository.findById(genre.getId()).orElse(null);
        assertNotNull(retrievedGenre);
        assertEquals("Sample Genre", retrievedGenre.getName());
    }

    @Test
    public void testSearchGenresByName() {
        // Create and save sample genres with different names
        Genre genre1 = new Genre();
        genre1.setName("Genre One");
        genreRepository.save(genre1);

        Genre genre2 = new Genre();
        genre2.setName("Genre Two");
        genreRepository.save(genre2);

        // Search for genres by name and assert the results
        List<Genre> genres = genreRepository.findByNameContaining("Genre");
        assertEquals(2, genres.size());
    }

    // Add more tests as needed
}

