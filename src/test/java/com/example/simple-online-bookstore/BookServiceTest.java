package test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class BookServiceTest {

    @Autowired
    private BookService bookService;

    @Test
    public void testCreateBook() {
        // Create a Book entity and save it using the service
        Book book = new Book();
        book.setTitle("Sample Book");
        Book createdBook = bookService.createBook(book);

        // Retrieve the created book and assert its attributes
        Book retrievedBook = bookService.getBookById(createdBook.getId());
        assertNotNull(retrievedBook);
        assertEquals("Sample Book", retrievedBook.getTitle());
    }

    @Test
    public void testSearchBooksByTitle() {
        // Create and save sample books with different titles
        Book book1 = new Book();
        book1.setTitle("Book One");
        bookService.createBook(book1);

        Book book2 = new Book();
        book2.setTitle("Book Two");
        bookService.createBook(book2);

        // Search for books by title using the service and assert the results
        List<Book> books = bookService.searchBooksByTitle("Book");
        assertEquals(2, books.size());
    }

    // Add more tests as needed
}

