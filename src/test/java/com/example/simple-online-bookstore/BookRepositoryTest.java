package test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void testCreateAndRetrieveBook() {
        // Create a Book entity and save it to the repository
        Book book = new Book();
        book.setTitle("Sample Book");
        bookRepository.save(book);

        // Retrieve the book from the repository and assert its attributes
        Book retrievedBook = bookRepository.findById(book.getId()).orElse(null);
        assertNotNull(retrievedBook);
        assertEquals("Sample Book", retrievedBook.getTitle());
    }

    @Test
    public void testSearchBooksByTitle() {
        // Create and save sample books with different titles
        Book book1 = new Book();
        book1.setTitle("Book One");
        bookRepository.save(book1);

        Book book2 = new Book();
        book2.setTitle("Book Two");
        bookRepository.save(book2);

        // Search for books by title and assert the results
        List<Book> books = bookRepository.findByTitleContaining("Book");
        assertEquals(2, books.size());
    }

    // Add more tests as needed
}
