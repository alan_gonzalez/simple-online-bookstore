package test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class AuthorRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void testCreateAndRetrieveAuthor() {
        // Create an Author entity and save it to the repository
        Author author = new Author();
        author.setName("Sample Author");
        authorRepository.save(author);

        // Retrieve the author from the repository and assert its attributes
        Author retrievedAuthor = authorRepository.findById(author.getId()).orElse(null);
        assertNotNull(retrievedAuthor);
        assertEquals("Sample Author", retrievedAuthor.getName());
    }

    @Test
    public void testSearchAuthorsByName() {
        // Create and save sample authors with different names
        Author author1 = new Author();
        author1.setName("Author One");
        authorRepository.save(author1);

        Author author2 = new Author();
        author2.setName("Author Two");
        authorRepository.save(author2);

        // Search for authors by name and assert the results
        List<Author> authors = authorRepository.findByNameContaining("Author");
        assertEquals(2, authors.size());
    }

    // Add more tests as needed
}
